#ifndef PhysicsObjectProxyBase_h
#define PhysicsObjectProxyBase_h

//MYCODE

//ROOT
#include "TLorentzVector.h"

//SYSTTEM
#include <string>
#include <vector>
#include <map>
#include <cfloat>
#include <climits>
#include <string>

class PhysObjBase: public TObject
{
public:
    PhysObjBase() {};
    ~PhysObjBase() {};

public:
    TLorentzVector p4;

private:
    ClassDef(PhysObjBase,1);
};

class myVertex: public PhysObjBase
{
public:
    myVertex(){};

public:
    int   vertexType;
    float x            ;
    float y            ;
    float z            ;

private:
    ClassDef(myVertex,1);
};

class myJet: public PhysObjBase
{
public:
    myJet(){};

public:
    bool IsSignal       = false;
    bool BTagged        = false;
    int  Quality        = -INT_MAX;
    int  TruthLabelID   = -INT_MAX;

    void SetBTagged(bool flag = true) { BTagged = flag; }

private:
    ClassDef(myJet,2);
};

class myElectron: public PhysObjBase
{
public:
    myElectron(){};

public:
    bool  IsSignal ;
    int   Quality  ;
    bool  IsGoodIP ;
    float Charge   ;
    float TriggerSF;

    std::map<std::string, bool> triggerMatching;

    std::vector<size_t> index_IDTrack;
    //size_t index_EMCalo;

private:
    ClassDef(myElectron,1);
};

class myMuon: public PhysObjBase
{
public:
    myMuon(){};

public:
    bool  IsSignal ;
    int   Quality  ;
    bool  IsGoodIP ;
    float Charge   ;
    float TriggerSF;

    std::map<std::string, bool> triggerMatching;

    std::vector<size_t> index_IDTrack;
    //size_t index_MSTrack;

private:
    ClassDef(myMuon,1);
};

class myTau: public PhysObjBase
{
public:
    myTau(){};

public:
    bool  IsSignal ;
    int   Quality  ;
    bool  IsGoodIP ;
    float Charge   ;
    float TriggerSF;

    std::map<std::string, bool> triggerMatching;

    std::vector<size_t> index_IDTrack;

private:
    ClassDef(myTau,1);
};


class myCaloCluster: public PhysObjBase
{
public:
    myCaloCluster(){};

public:
    float ClusterSize;

private:
    ClassDef(myCaloCluster,1);
};

class myMET: public PhysObjBase
{
public:
    myMET(){};

public:
    std::map<std::string, TVector2> term;

private:
    ClassDef(myMET,1);
};


class myTruth: public PhysObjBase
{
public:
    myTruth(){};

public:
    float          qoverp         ;
    float          qoverpt        ;
    float          ProperTime     ;
    float          Mass           ;
    float          Charge         ;
    float          ProdRadius     ;
    float          DecayRadius    ;
    int            nChildren      ;
    bool           FromZ          ;
    float          PdgId          ;
    std::string    Type           ;
    bool           DAODtruth      ;
    std::string    Origin         ;
    bool           ORtruthJet     ;
    double pt_vis;
    double eta_vis;
    double phi_vis;
    double m_vis;
    size_t numCharged;
    size_t numChargedPion;

    bool            HasHadronInChildren   ;
    bool            HasElectronInChildren ;
    bool            HasMuonInChildren     ;
    bool            HasPhotonInChildren   ;

    //=== Scattering chain (following only lepon) ===//
    std::vector<myTruth>   DecayParticle;
    std::vector<float>     DecayParticle_dR;
    std::vector<float>     DecayParticle_dPt;

    //=== Child particles (only 1 decay. Not record the grandchild) ===//
    std::vector<myTruth> ChildParticle;

private:
    ClassDef(myTruth,1);
};

class myTrack: public PhysObjBase
{
public:
    myTrack(){};

public:
    //=== Track type ===//
    bool ReTracking       ;
    bool PixelTracklet    ;
    //bool MSTrack          ;

    //=== Perigee parameter ===//
    float d0              ;
    float z0              ;
    float theta           ;
    float phi             ;
    float qoverp          ;
    float qoverpt         ;
    float d0wrtCV         ;
    float d0sigToolwrtCV  ;
    float z0wrtPV         ;
    float z0wrtCV         ;
    float z0sinthetawrtPV ;
    float z0sinthetawrtCV ;
    float beamspotz       ;

    float d0err              ;
    float z0err              ;
    float thetaerr           ;
    float phierr             ;
    float qoverperr          ;
    float z0wrtPVerr         ;

    float z0wrtPVsig         ;
    float z0sinthetawrtPVsig ;
    float d0sigTool          ;
    float d0sigToolBeamErr   ;
    float BeamErr            ;

    float charge             ;

    //=== dE/dx ===//
    float pixeldEdx      ;

    //=== Fit Quality ===//
    float chiSquared     ;
    float Quality        ;
    float chi2OvernDoF   ;
    int   nDoF           ;

    //=== TruthMatching Probability ===//
    float truthMatchingProbability ;

    //=== Isolation ===//
    float ptcone20_1gev         ;
    float ptcone30_1gev         ;
    float ptcone40_1gev         ;
    float ptcone20overPt_1gev   ;
    float ptcone30overPt_1gev   ;
    float ptcone40overPt_1gev   ;
    float ptvarcone20           ;
    float ptvarcone30           ;
    float ptvarcone40           ;
    float ptvarcone20overPt     ;
    float ptvarcone30overPt     ;
    float ptvarcone40overPt     ;
    float etcone20_topo         ;
    float etcone30_topo         ;
    float etcone40_topo         ;
    float etcone20overPt_topo   ;
    float etcone30overPt_topo   ;
    float etcone40overPt_topo   ;
    float etclus20_topo         ;
    float etclus30_topo         ;
    float etclus40_topo         ;
    float etclus20overPt_topo   ;
    float etclus30overPt_topo   ;
    float etclus40overPt_topo   ;
    float nearestClusterdR_calo ;
    float nearestClusteret_calo ;
    float nearestClusterdR_topo ;
    float nearestClusteret_topo ;

    //=== Overlap ===//
    float dRJet20                ;
    float dRJet50                ;
    float dRElectron             ;
    float dRMuon                 ;
    float dRMSTrack              ;
    float dRTrack_5gev           ;
    float dRTrack_10gev          ;

    //=== Vertex constraint ===//
    float KVUPt                         ;
    float KVUEta                        ;
    float KVUPhi                        ;
    float KVUd0                         ;
    float KVUz0                         ;
    float KVUtheta                      ;
    float KVUphi                        ;
    float KVUqoverp                     ;
    float KVUqoverpt                    ;
    float KVUz0wrtPV                    ;
    float KVUz0sinthetawrtPV            ;
    float KVUd0err                      ;
    float KVUz0err                      ;
    float KVUthetaerr                   ;
    float KVUphierr                     ;
    float KVUqoverperr                  ;
    float KVUz0wrtPVerr                 ;
    float KVUz0wrtPVsig                 ;
    float KVUz0sinthetawrtPVsig         ;
    float KVUd0sigTool                  ;
    float KVUd0sigToolBeamErr           ;
    float KVUBeamErr                    ;
    float KVUchiSquared                 ;
    float KVUQuality                    ;
    float KVUchi2OvernDoF               ;
    float KVUvertexQuality              ;
    float KVUvertexchi2OvernDoF         ;
    int   KVUvertexType                 ;
    float KVUvertexx                    ;
    float KVUvertexy                    ;
    float KVUvertexz                    ;
    int   KVUvertexnParticles           ;
    float KVUvertexchiSquared           ;
    float KVUvertexnumberDoF            ;
    float KVUvertexminIP                ;
    float KVUvertexIPd0                 ;
    float KVUvertexIPz0                 ;
    float KVUvertexPVIP                 ;
    float KVUvertexPVIPd0               ;
    float KVUvertexPVIPz0               ;

    //=== Smalld0 ===//
    float Smalld0KVUPt                         ;
    float Smalld0KVUEta                        ;
    float Smalld0KVUPhi                        ;
    float Smalld0KVUd0                         ;
    float Smalld0KVUz0                         ;
    float Smalld0KVUtheta                      ;
    float Smalld0KVUphi                        ;
    float Smalld0KVUqoverp                     ;
    float Smalld0KVUqoverpt                    ;
    float Smalld0KVUz0wrtPV                    ;
    float Smalld0KVUz0sinthetawrtPV            ;
    float Smalld0KVUchiSquared                 ;
    float Smalld0KVUQuality                    ;
    float Smalld0KVUchi2OvernDoF               ;
    float Smalld0KVUvertexQuality              ;
    float Smalld0KVUvertexchi2OvernDoF         ;
    int   Smalld0KVUvertexType                 ;
    float Smalld0KVUvertexx                    ;
    float Smalld0KVUvertexy                    ;
    float Smalld0KVUvertexz                    ;
    int   Smalld0KVUvertexnParticles           ;
    float Smalld0KVUvertexchiSquared           ;
    float Smalld0KVUvertexnumberDoF            ;


    //=== Number of hits ===//
    int numberOfContribPixelLayers    ;
    int nBLayerHits                   ;
    int nPixHits                      ;
    int nSCTHits                      ;
    int nTRTHits                      ;
    int nSiHits                       ;

    int nBLayerSharedHits             ;
    int nPixelSharedHits              ;
    int nSCTSharedHits                ;
    int nTRTSharedHits                ;
    int nSiSharedHits                 ;

    int nPixelHoles                   ;
    int nSCTHoles                     ;
    int nSCTDoubleHoles               ;
    int nTRTHoles                     ;
    int nSiHoles                      ;

    int nBLayerOutliers               ;
    int nPixelOutliers                ;
    int nSCTOutliers                  ;
    int nTRTOutliers                  ;
    int nSiOutliers                   ;

    int nBLayerHitsPlusOutlier        ;
    int nPixHitsPlusOutlier           ;
    int nSCTHitsPlusOutlier           ;
    int nTRTHitsPlusOutlier           ;
    int nSiHitsPlusOutlier            ;

    int numberOfPixelSpoiltHits       ;
    int numberOfSCTSpoiltHits         ;

    int expectBLayerHit                   ;
    int expectInnermostPixelLayerHit      ;
    int numberOfInnermostPixelLayerHits   ;

    int numberOfGangedPixels          ;
    int numberOfGangedFlaggedFakes    ;

    int numberOfPixelDeadSensors      ;
    int numberOfSCTDeadSensors        ;
    int numberOfTRTDeadStraws         ;

    int MuonSegment40_nPrecisionHits  ;
    int MuonSegment40_nPhiLayers      ;
    int MuonSegment40_nTrigEtaLayers  ;
    int MuonSegment40_nAllHits        ;

    //=== Flag of selection ===//
    bool IsPassedSR               ;
    bool IsPassedFakeCR           ;
    bool IsPassedHadCR            ;
    bool IsPassedEleCRTag         ;
    bool IsPassedEleCRProbe       ;
    bool IsPassedMuCRTag          ;
    bool IsPassedMuCRProbe        ;

    bool IsPassedIsolated         ;
    bool IsPassedIsolatedLeading  ;
    bool IsPassedORJet            ;
    bool IsPassedORLep            ;
    bool IsPassedQuality          ;
    bool IsPassedBadHitsVeto      ;
    bool IsPassedEta              ;
    bool IsPassedImpactParameter  ;
    bool IsPassedTRTVeto          ;
    bool IsPassedSCTVeto          ;

    //=== Hit Pattern ===//
    bool pixelBarrel0     ;
    bool pixelBarrel1     ;
    bool pixelBarrel2     ;
    bool pixelBarrel3     ;
    bool pixelEndCap0     ;
    bool pixelEndCap1     ;
    bool pixelEndCap2     ;
    bool sctBarrel0       ;
    bool sctBarrel1       ;
    bool sctBarrel2       ;
    bool sctBarrel3       ;
    bool sctEndCap0       ;
    bool sctEndCap1       ;
    bool sctEndCap2       ;
    bool sctEndCap3       ;
    bool sctEndCap4       ;
    bool sctEndCap5       ;
    bool sctEndCap6       ;
    bool sctEndCap7       ;
    bool sctEndCap8       ;
    bool trtBarrel        ;
    bool trtEndCap        ;
    bool DBM0             ;
    bool DBM1             ;
    bool DBM2             ;
    
    //=== Truth Information ===//
    float DecayRadius     ;
    float DecayProperTime ;

    float deltaPt         ;
    float deltaQoverP     ;
    float deltaQoverPt    ;
    float deltaEta        ;
    float deltaTheta      ;
    float deltaPhi        ;
    float deltaR          ;
    float deltaQoverPSig  ;
    float deltaThetaSig   ;
    float deltaPhiSig     ;

    float TruthPt         ;
    float TruthEta        ;
    float TruthPhi        ;
    float TruthCharge     ;
    float TruthMass       ;
    float TruthFromZ      ;

    size_t index_Truth    ;

    std::string TruthType   ;
    bool   TruthDAODtruth   ;
    bool   TruthORtruthJet  ;
    std::string TruthOrigin ;

    //=== Systematic (Overlap variables) ===//
    std::vector<std::string> syst_name;
    std::vector<float>       syst_dRJet20;
    std::vector<float>       syst_dRJet50;
    std::vector<float>       syst_dRElectron;
    std::vector<float>       syst_dRMuon;
    std::vector<float>       syst_dRMSTrack;

private:
    ClassDef(myTrack,1);
};

class myZee: public PhysObjBase
{
public:
    myZee(){};

public:
    // myElectron      TagLepton;
    // myTrack         ProbeIdTrack;
    // myCaloCluster   ProbeCaloCluster;
    size_t      index_TagLepton       ;
    size_t      index_ProbeIdTrack    ;
    size_t      index_ProbeCaloCluster;

private:
    ClassDef(myZee,1);
};


class myZmumu: public PhysObjBase
{
public:
    myZmumu(){};

public:
    // myMuon      TagLepton;
    // myTrack     ProbeIdTrack;
    // myTrack     ProbeMSTrack;
    size_t     index_TagLepton   ;
    size_t     index_ProbeIdTrack;
    size_t     index_ProbeMSTrack;

private:
    ClassDef(myZmumu,1);
};

#endif
