//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Oct  8 15:27:32 2021 by ROOT version 6.22/08
// from TChain all_tracklet_tree/
//////////////////////////////////////////////////////////

#ifndef acceptance_challenge_h
#define acceptance_challenge_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

struct track {
  Float_t         pt;
  Float_t         eta;
  Float_t         phi;
  Float_t         mass;
  Float_t         energy;
  Float_t         charge;
  Int_t           pdgid;
  Float_t         qoverp;
  Float_t         phi0;
  Float_t         theta;
  Float_t         d0;
  Float_t         z0;
  Float_t         z0ST;
  Float_t         d0sig;
  Float_t         z0sig;
  Float_t         z0STsig;
  Float_t         d0sig_PV;
  Float_t         z0_PV;
  Float_t         z0sig_PV;
  Float_t         z0ST_PV;
  Float_t         z0STsig_PV;
  Float_t         chi2;
  Float_t         chi2_prob;
  Int_t           ndof;
  Int_t           overlap_jet20;
  Int_t           overlap_jet50;
  Int_t           overlap_muon;
  Int_t           overlap_electron;
  Int_t           overlap_ms_track;
  Int_t           overlap_std_track;
  Float_t         vc_qoverp;
  Float_t         vc_phi0;
  Float_t         vc_theta;
  Float_t         vc_chi2;
  Float_t         vc_chi2_prob;
  Float_t         vc_pt;
  Float_t         vc_eta;
  Float_t         truth_pt;
  Float_t         truth_eta;
  Float_t         truth_phi;
  Float_t         truth_mass;
  Float_t         truth_energy;
  Float_t         truth_charge;
  Float_t         truth_dR;
  Int_t           truth_pdgid;
  Int_t           truth_type;
  Float_t         truth_decayradius;
  Float_t         truth_propertime;
  Float_t         std_track_dR;
  Float_t         tracklet_dR;
  Float_t         jet20_dR;
  Float_t         jet50_dR;
  Float_t         dEdx;
  Float_t         iso_vctrk20;
  Float_t         iso_vctrk30;
  Float_t         iso_vctrk40;
  Float_t         iso_trk20;
  Float_t         iso_trk30;
  Float_t         iso_trk40;
  Float_t         iso_etcone20;
  Float_t         iso_etcone30;
  Float_t         iso_etcone40;
  Float_t         iso_etclus20;
  Float_t         iso_etclus30;
  Float_t         iso_etclus40;
  Int_t           pixcontib;
  Int_t           ganged;
  Int_t           pixspoilt;
  Int_t           pix_hit;
  Int_t           pix_hole;
  Int_t           pix_shared;
  Int_t           pix_outlier;
  Int_t           pix_dead;
  Int_t           sct_hit;
  Int_t           sct_hole;
  Int_t           sct_shared;
  Int_t           sct_outlier;
  Int_t           sct_dead;
  Int_t           trt_hit;
  Int_t           trt_hole;
  Int_t           trt_shared;
  Int_t           trt_outlier;
  Int_t           trt_dead;
  Int_t           hit_pattern_pixelBarrel0;
  Int_t           hit_pattern_pixelBarrel1;
  Int_t           hit_pattern_pixelBarrel2;
  Int_t           hit_pattern_pixelBarrel3;
  Int_t           hit_pattern_pixelEndCap0;
  Int_t           hit_pattern_pixelEndCap1;
  Int_t           hit_pattern_pixelEndCap2;
  Int_t           hit_pattern_sctBarrel0;
  Int_t           hit_pattern_sctBarrel1;
  Int_t           hit_pattern_sctBarrel2;
  Int_t           hit_pattern_sctBarrel3;
  Int_t           hit_pattern_sctEndCap0;
  Int_t           hit_pattern_sctEndCap1;
  Int_t           hit_pattern_sctEndCap2;
  Int_t           hit_pattern_sctEndCap3;
  Int_t           hit_pattern_sctEndCap4;
  Int_t           hit_pattern_sctEndCap5;
  Int_t           hit_pattern_sctEndCap6;
  Int_t           hit_pattern_sctEndCap7;
  Int_t           hit_pattern_sctEndCap8;
  Int_t           hit_pattern_trtBarrel;
  Int_t           hit_pattern_trtEndCap;
  Int_t           hit_pattern_DBM0;
  Int_t           hit_pattern_DBM1;
  Int_t           hit_pattern_DBM2;
  Int_t           hit_pattern;


  Float_t         DecayRadius       = 0.;
  Float_t         TruthDeltaR       = 0.;
  Bool_t          isMatched         = false;
  Float_t         MatchedTruthPt    = 0.;
  Float_t         MatchedTruthEta   = 0.;
  Float_t         MatchedTruthPhi   = 0.;
  Int_t           isIsolatedLeading = 0;
  Bool_t          isThreeLayer      = false;
  Bool_t          isFourLayer       = false;
  Bool_t          isMissingHit      = false;
  Bool_t          isLargeDistance0  = false;
  Int_t           MissingLayer      = -1;

  Double_t        min_deltaR_jet    = DBL_MAX;
};

struct truth {
  Float_t         pt;
  Float_t         eta;
  Float_t         phi;
  Float_t         mass;
  Float_t         energy;
  Float_t         charge;
  Int_t           pdgid;
  Float_t         decayradius;
  Float_t         propertime;
};

struct jet {
  Float_t         pt;
  Float_t         eta;
  Float_t         phi;
  Float_t         mass;
  Float_t         energy;
  Float_t         charge;
  Int_t           pdgid;
  Float_t         jvt;
  Float_t         btag;
  Float_t         is_btag;
};

struct electron {
  Float_t         pt;
  Float_t         eta;
  Float_t         phi;
  Float_t         mass;
  Float_t         energy;
  Float_t         charge;
  Int_t           pdgid;
  Int_t           type;
  Int_t           id_level;
};

struct muon {
  Float_t         pt;
  Float_t         eta;
  Float_t         phi;
  Float_t         mass;
  Float_t         energy;
  Float_t         charge;
  Int_t           pdgid;
  Int_t           type;
  Int_t           id_level;
};

// Header file for the classes stored in the TTree if any.

class acceptance_challenge {
 public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  unsigned int max_track    = 30;
  unsigned int max_jet      = 5;
  unsigned int max_electron = 2;
  unsigned int max_muon     = 2;
  double EventWeight;
  vector<struct track>    track_copy    = vector<struct track>   (max_track);
  vector<struct jet>      jet_copy      = vector<struct jet>     (max_jet);
  vector<struct electron> electron_copy = vector<struct electron>(max_electron);
  vector<struct muon>     muon_copy     = vector<struct muon>    (max_muon);

  // Fixed size dimensions of array or collections stored in the TTree if any.
  TH1D* h_event_cutflow                  = new TH1D("cut_flow_event"                 , ";;Entry", 4 , 0.,  4.);
  TH1D* h_cutflow                        = new TH1D("cut_flow"                       , ";;Entry", 16, 0., 16.);
  TH1D* h_cutflow_3L                     = new TH1D("cut_flow_3L"                    , ";;Entry", 16, 0., 16.);
  TH1D* h_cutflow_4L                     = new TH1D("cut_flow_4L"                    , ";;Entry", 16, 0., 16.);
  TH1D* h_matched_cutflow_3L             = new TH1D("matched_cut_flow_3L"            , ";;Entry", 16, 0., 16.);
  TH1D* h_matched_cutflow_4L             = new TH1D("matched_cut_flow_4L"            , ";;Entry", 16, 0., 16.);
  TH1D* h_objectlevel_cutflow_3L         = new TH1D("objectlevel_cut_flow_3L"        , ";;Entry", 16, 0., 16.);
  TH1D* h_objectlevel_cutflow_4L         = new TH1D("objectlevel_cut_flow_4L"        , ";;Entry", 16, 0., 16.);
  TH1D* h_objectlevel_matched_cutflow_3L = new TH1D("objectlevel_matched_cut_flow_3L", ";;Entry", 16, 0., 16.);
  TH1D* h_objectlevel_matched_cutflow_4L = new TH1D("objectlevel_matched_cut_flow_4L", ";;Entry", 16, 0., 16.);
  TH2D* h_number_cutflow_3L              = new TH2D("number_cut_flow_3L"             , ";;Entry", 16, 0., 16., 10, 1., 11.);
  TH2D* h_number_cutflow_4L              = new TH2D("number_cut_flow_4L"             , ";;Entry", 16, 0., 16., 10, 1., 11.);

  //vector<int> ntracklet_all_cutflow;
  vector<int> n_tracklet_cutflow ;
  vector<int> n_tracklet_four_layer_cutflow ;
  vector<int> n_tracklet_three_layer_cutflow;
  vector<int> n_matched_tracklet_four_layer_cutflow ;
  vector<int> n_matched_tracklet_three_layer_cutflow;

  // Declaration of leaf types
  Float_t         event_info_weight_mc;
  Float_t         event_info_weight_pu;
  Float_t         event_info_weight_xsec;
  Float_t         event_info_weight_trigger;
  Float_t         event_info_weight_lepton;
  Float_t         event_info_weight_other;
  Float_t         event_info_weight_sherpa;
  Float_t         event_info_weight_all;
  Int_t           event_info_avg_int_per_xing;
  Int_t           event_info_act_int_per_xing;
  Int_t           event_info_avg_int_per_xing_corr;
  Int_t           event_info_n_primary_vertex_3trk;
  Int_t           event_info_n_primary_vertex_4trk;
  Int_t           event_info_n_primary_vertex_5trk;
  UInt_t          event_info_is_data;
  ULong64_t       event_info_event_number;
  UInt_t          event_info_run_number;
  UInt_t          event_info_lumi_block;
  UInt_t          event_info_run_number_random;
  UInt_t          event_info_event_cleaning;
  UInt_t          event_info_lepton_veto;
  UInt_t          event_info_number_of_tracklets;
  UInt_t          event_info_number_of_jets_pt100;
  UInt_t          event_info_number_of_tracklets_pt20;
  UInt_t          event_info_number_of_tracklets_vcpt20;
  UInt_t          event_info_number_of_electrons;
  UInt_t          event_info_number_of_muons;
  Int_t           electron_trigger_fire;
  Int_t           electron_trigger_match;
  Float_t         electron_trigger_eff;
  Float_t         electron_trigger_sf;
  Int_t           muon_trigger_fire;
  Int_t           muon_trigger_match;
  Float_t         muon_trigger_eff;
  Float_t         muon_trigger_sf;
  Int_t           met_trigger_fire;
  Int_t           met_trigger_match;
  Float_t         met_trigger_eff;
  Float_t         met_trigger_sf;
  Float_t         vertex_x;
  Float_t         vertex_y;
  Float_t         vertex_z;
  Float_t         vertex_x_sig;
  Float_t         vertex_y_sig;
  Float_t         vertex_z_sig;
  Float_t         vertex_sum_pt2;
  Float_t         vertex_chi2;
  Float_t         vertex_chi2_prob;
  Float_t         vertex_ndof;
  Float_t         vertex_n_trk;
  Int_t           vertex_vtx_type;
  Float_t         missingET_ET;
  Float_t         missingET_phi;
  Float_t         missingET_px;
  Float_t         missingET_py;
  Float_t         missingET_sumET;
  Float_t         missingET_sig;

  Float_t         p_truth_pt;
  Float_t         p_truth_eta;
  Float_t         p_truth_phi;
  Float_t         p_truth_mass;
  Float_t         p_truth_energy;
  Float_t         p_truth_charge;
  Int_t           p_truth_pdgid;
  Float_t         p_truth_decayradius;
  Float_t         p_truth_propertime;
  Float_t         m_truth_pt;
  Float_t         m_truth_eta;
  Float_t         m_truth_phi;
  Float_t         m_truth_mass;
  Float_t         m_truth_energy;
  Float_t         m_truth_charge;
  Int_t           m_truth_pdgid;
  Float_t         m_truth_decayradius;
  Float_t         m_truth_propertime;


  // List of branches
  TBranch        *b_event_info_weight_mc;   //!
  TBranch        *b_event_info_weight_pu;   //!
  TBranch        *b_event_info_weight_xsec;   //!
  TBranch        *b_event_info_weight_trigger;   //!
  TBranch        *b_event_info_weight_lepton;   //!
  TBranch        *b_event_info_weight_other;   //!
  TBranch        *b_event_info_weight_sherpa;   //!
  TBranch        *b_event_info_weight_all;   //!
  TBranch        *b_event_info_avg_int_per_xing;   //!
  TBranch        *b_event_info_act_int_per_xing;   //!
  TBranch        *b_event_info_avg_int_per_xing_corr;   //!
  TBranch        *b_event_info_n_primary_vertex_3trk;   //!
  TBranch        *b_event_info_n_primary_vertex_4trk;   //!
  TBranch        *b_event_info_n_primary_vertex_5trk;   //!
  TBranch        *b_event_info_is_data;   //!
  TBranch        *b_event_info_event_number;   //!
  TBranch        *b_event_info_run_number;   //!
  TBranch        *b_event_info_lumi_block;   //!
  TBranch        *b_event_info_run_number_random;   //!
  TBranch        *b_event_info_event_cleaning;   //!
  TBranch        *b_event_info_lepton_veto;   //!
  TBranch        *b_event_info_number_of_tracklets;   //!
  TBranch        *b_event_info_number_of_jets_pt100;   //!
  TBranch        *b_event_info_number_of_tracklets_pt20;   //!
  TBranch        *b_event_info_number_of_tracklets_vcpt20;   //!
  TBranch        *b_event_info_number_of_electrons;   //!
  TBranch        *b_event_info_number_of_muons;   //!
  TBranch        *b_electron_trigger_fire;   //!
  TBranch        *b_electron_trigger_match;   //!
  TBranch        *b_electron_trigger_eff;   //!
  TBranch        *b_electron_trigger_sf;   //!
  TBranch        *b_muon_trigger_fire;   //!
  TBranch        *b_muon_trigger_match;   //!
  TBranch        *b_muon_trigger_eff;   //!
  TBranch        *b_muon_trigger_sf;   //!
  TBranch        *b_met_trigger_fire;   //!
  TBranch        *b_met_trigger_match;   //!
  TBranch        *b_met_trigger_eff;   //!
  TBranch        *b_met_trigger_sf;   //!
  TBranch        *b_vertex_x;   //!
  TBranch        *b_vertex_y;   //!
  TBranch        *b_vertex_z;   //!
  TBranch        *b_vertex_x_sig;   //!
  TBranch        *b_vertex_y_sig;   //!
  TBranch        *b_vertex_z_sig;   //!
  TBranch        *b_vertex_sum_pt2;   //!
  TBranch        *b_vertex_chi2;   //!
  TBranch        *b_vertex_chi2_prob;   //!
  TBranch        *b_vertex_ndof;   //!
  TBranch        *b_vertex_n_trk;   //!
  TBranch        *b_vertex_vtx_type;   //!
  TBranch        *b_missingET_ET;   //!
  TBranch        *b_missingET_phi;   //!
  TBranch        *b_missingET_px;   //!
  TBranch        *b_missingET_py;   //!
  TBranch        *b_missingET_sumET;   //!
  TBranch        *b_missingET_sig;   //!

  vector<TBranch*>        b_track_pt = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_eta = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_phi = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_mass = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_energy = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_charge = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_pdgid = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_qoverp = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_phi0 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_theta = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_d0 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_z0 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_z0ST = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_d0sig = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_z0sig = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_z0STsig = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_d0sig_PV = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_z0_PV = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_z0sig_PV = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_z0ST_PV = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_z0STsig_PV = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_chi2 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_chi2_prob = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_ndof = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_overlap_jet20 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_overlap_jet50 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_overlap_muon = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_overlap_electron = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_overlap_ms_track = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_overlap_std_track = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_vc_qoverp = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_vc_phi0 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_vc_theta = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_vc_chi2 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_vc_chi2_prob = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_vc_pt = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_vc_eta = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_pt = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_eta = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_phi = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_mass = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_energy = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_charge = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_dR = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_pdgid = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_type = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_decayradius = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_truth_propertime = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_std_track_dR = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_tracklet_dR = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_jet20_dR = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_jet50_dR = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_dEdx = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_vctrk20 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_vctrk30 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_vctrk40 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_trk20 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_trk30 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_trk40 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_etcone20 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_etcone30 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_etcone40 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_etclus20 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_etclus30 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_iso_etclus40 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_pixcontib = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_ganged = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_pixspoilt = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_pix_hit = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_pix_hole = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_pix_shared = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_pix_outlier = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_pix_dead = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_sct_hit = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_sct_hole = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_sct_shared = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_sct_outlier = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_sct_dead = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_trt_hit = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_trt_hole = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_trt_shared = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_trt_outlier = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_trt_dead = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_pixelBarrel0 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_pixelBarrel1 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_pixelBarrel2 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_pixelBarrel3 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_pixelEndCap0 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_pixelEndCap1 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_pixelEndCap2 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctBarrel0 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctBarrel1 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctBarrel2 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctBarrel3 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctEndCap0 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctEndCap1 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctEndCap2 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctEndCap3 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctEndCap4 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctEndCap5 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctEndCap6 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctEndCap7 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_sctEndCap8 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_trtBarrel = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_trtEndCap = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_DBM0 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_DBM1 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern_DBM2 = vector<TBranch*>(max_track);   //!
  vector<TBranch*>        b_track_hit_pattern = vector<TBranch*>(max_track);   //!

  TBranch        *b_p_truth_pt;   //!
  TBranch        *b_p_truth_eta;   //!
  TBranch        *b_p_truth_phi;   //!
  TBranch        *b_p_truth_mass;   //!
  TBranch        *b_p_truth_energy;   //!
  TBranch        *b_p_truth_charge;   //!
  TBranch        *b_p_truth_pdgid;   //!
  TBranch        *b_p_truth_decayradius;   //!
  TBranch        *b_p_truth_propertime;   //!
  TBranch        *b_m_truth_pt;   //!
  TBranch        *b_m_truth_eta;   //!
  TBranch        *b_m_truth_phi;   //!
  TBranch        *b_m_truth_mass;   //!
  TBranch        *b_m_truth_energy;   //!
  TBranch        *b_m_truth_charge;   //!
  TBranch        *b_m_truth_pdgid;   //!
  TBranch        *b_m_truth_decayradius;   //!
  TBranch        *b_m_truth_propertime;   //!


  vector<TBranch*>        b_jet_pt = vector<TBranch*>(max_jet);   //!
  vector<TBranch*>        b_jet_eta = vector<TBranch*>(max_jet);   //!
  vector<TBranch*>        b_jet_phi = vector<TBranch*>(max_jet);   //!
  vector<TBranch*>        b_jet_mass = vector<TBranch*>(max_jet);   //!
  vector<TBranch*>        b_jet_energy = vector<TBranch*>(max_jet);   //!
  vector<TBranch*>        b_jet_charge = vector<TBranch*>(max_jet);   //!
  vector<TBranch*>        b_jet_pdgid = vector<TBranch*>(max_jet);   //!
  vector<TBranch*>        b_jet_jvt = vector<TBranch*>(max_jet);   //!
  vector<TBranch*>        b_jet_btag = vector<TBranch*>(max_jet);   //!
  vector<TBranch*>        b_jet_is_btag = vector<TBranch*>(max_jet);   //!

  vector<TBranch*>        b_electron_pt = vector<TBranch*>(max_electron);   //!
  vector<TBranch*>        b_electron_eta = vector<TBranch*>(max_electron);   //!
  vector<TBranch*>        b_electron_phi = vector<TBranch*>(max_electron);   //!
  vector<TBranch*>        b_electron_mass = vector<TBranch*>(max_electron);   //!
  vector<TBranch*>        b_electron_energy = vector<TBranch*>(max_electron);   //!
  vector<TBranch*>        b_electron_charge = vector<TBranch*>(max_electron);   //!
  vector<TBranch*>        b_electron_pdgid = vector<TBranch*>(max_electron);   //!
  vector<TBranch*>        b_electron_type = vector<TBranch*>(max_electron);   //!
  vector<TBranch*>        b_electron_id_level = vector<TBranch*>(max_electron);   //!

  vector<TBranch*>        b_muon_pt = vector<TBranch*>(max_muon);   //!
  vector<TBranch*>        b_muon_eta = vector<TBranch*>(max_muon);   //!
  vector<TBranch*>        b_muon_phi = vector<TBranch*>(max_muon);   //!
  vector<TBranch*>        b_muon_mass = vector<TBranch*>(max_muon);   //!
  vector<TBranch*>        b_muon_energy = vector<TBranch*>(max_muon);   //!
  vector<TBranch*>        b_muon_charge = vector<TBranch*>(max_muon);   //!
  vector<TBranch*>        b_muon_pdgid = vector<TBranch*>(max_muon);   //!
  vector<TBranch*>        b_muon_type = vector<TBranch*>(max_muon);   //!
  vector<TBranch*>        b_muon_id_level = vector<TBranch*>(max_muon);   //!


  acceptance_challenge(TTree *tree=0);
  virtual ~acceptance_challenge();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
  virtual vector<struct truth> SetTruthObject();
  virtual std::string replaceStr(std::string&, std::string, std::string);
  virtual void     fill_cutflow(int, struct track);
};

#endif
